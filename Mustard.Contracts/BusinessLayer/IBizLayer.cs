﻿namespace Mustard.Contracts.BusinessLayer
{
  public interface IBizLayer
  {
    bool CanAssignOrderToManager(string managerUserId);
    bool CanAddSlaveToManager(string managerUserId);
  }
}