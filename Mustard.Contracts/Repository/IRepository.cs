﻿namespace Mustard.Contracts.Repository
{
  public interface IRepository
  {
    void ApplyChanges();
  }
}