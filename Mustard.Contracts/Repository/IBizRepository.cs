﻿using System.Linq;
using Mustard.Models;

namespace Mustard.Contracts.Repository
{
  public interface IBizRepository : IRepository
  {
    #region Order

    IQueryable<Order> Orders { get; }
    bool CreateOrder(Order instance);
    bool UpdateOrder(Order instance);
    bool RemoveOrder(int idOrder);

    #endregion

    #region CustomerCard

    IQueryable<CustomerCard> CustomerCards { get; }
    bool CreateCustomerCard(CustomerCard instance);
    bool UpdateCustomerCard(CustomerCard instance);
    bool RemoveCustomerCard(string idCustomerCard);

    #endregion

    #region EmployeeCard

    IQueryable<EmployeeCard> EmployeeCards { get; }
    bool CreateEmployeeCard(EmployeeCard instance);
    bool UpdateEmployeeCard(EmployeeCard instance);
    bool RemoveEmployeeCard(string idEmployeeCard);

    #endregion
  }
}