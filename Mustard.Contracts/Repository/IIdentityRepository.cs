﻿using System.Linq;
using Mustard.Models;

namespace Mustard.Contracts.Repository
{
  public interface IIdentityRepository : IRepository
  {
    #region User

    IQueryable<User> Users { get; }
    bool CreateUser(User instance);
    bool UpdateUser(User instance);
    bool RemoveUser(string idUser);

    #endregion

    #region Role

    IQueryable<Role> Roles { get; }
    bool CreateRole(Role instance);
    bool UpdateRole(Role instance);
    bool RemoveRole(string idRole);

    #endregion

    #region PasswordResetToken

    IQueryable<PasswordResetToken> PasswordResetTokens { get; }
    bool CreatePasswordResetToken(PasswordResetToken instance);
    bool UpdatePasswordResetToken(PasswordResetToken instance);
    bool RemovePasswordResetToken(string idPasswordResetToken);

    #endregion
  }
}