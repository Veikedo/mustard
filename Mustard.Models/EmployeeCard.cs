﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mustard.Models
{
  public class EmployeeCard
  {
    public EmployeeCard()
    {
      Slaves = new Collection<EmployeeCard>();
      Orders = new Collection<Order>();
    }

    [Key, ForeignKey("User")]
    public string UserId { get; set; }

    public virtual User User { get; set; }

    [ForeignKey("ManagerCardId")]
    public virtual EmployeeCard ManagerCard { get; set; }

    public string ManagerCardId { get; set; }

    public int Version { get; set; }
    public string Name { get; set; }
    public string Surname { get; set; }
    public virtual ICollection<EmployeeCard> Slaves { get; private set; }
    public virtual ICollection<Order> Orders { get; private set; }
  }
}