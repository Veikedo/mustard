﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace Mustard.Models
{
  public class Role
  {
    public Role()
    {
      Users = new Collection<User>();
      Id = Guid.NewGuid().ToString("N");
    }

    public string Id { get; set; }

    [Required, MinLength(1)]
    public string Code { get; set; }

    public virtual ICollection<User> Users { get; private set; }
  }
}