﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mustard.Models
{
  public class Order
  {
    public Order()
    {
      CustomerCardId = Guid.NewGuid().ToString("N");
      ManagerCardId = Guid.NewGuid().ToString("N");
    }

    public int Id { get; set; }
    public string Description { get; set; }
    public string CustomerCardId { get; set; }
    public string ManagerCardId { get; set; }

    [ForeignKey("CustomerCardId")]
    public virtual CustomerCard CustomerCard { get; set; }

    [ForeignKey("ManagerCardId")]
    public virtual EmployeeCard ManagerCard { get; set; }
  }
}