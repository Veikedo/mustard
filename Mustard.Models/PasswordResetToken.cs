﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mustard.Models
{
  public class PasswordResetToken
  {
    [Key, ForeignKey("User")]
    public string UserId { get; set; }

    public virtual User User { get; set; }
    public string Token { get; set; }
  }
}