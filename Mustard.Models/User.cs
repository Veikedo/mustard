﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Mustard.Models
{
  public class User
  {
    public User()
    {
      Id = Guid.NewGuid().ToString("N");
      Roles = new Collection<Role>();
    }

    public string Id { get; set; }
    public string UserName { get; set; }
    public string PasswordHash { get; set; }
    public string Email { get; set; }
    public virtual ICollection<Role> Roles { get; private set; }
  }
}