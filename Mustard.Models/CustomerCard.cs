﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mustard.Models
{
  public class CustomerCard
  {
    public CustomerCard()
    {
      Orders = new Collection<Order>();
    }

    [Key, ForeignKey("User")]
    public string UserId { get; set; }

    public virtual User User { get; set; }

    [Required, MinLength(1)]
    public string CompanyName { get; set; }

    public string Street { get; set; }
    public string House { get; set; }
    public virtual ICollection<Order> Orders { get; private set; }
  }
}