using System;
using System.Web;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Mustard.Application;
using Mustard.Application.Annotations;
using Mustard.BusinessLayer;
using Mustard.Contracts.BusinessLayer;
using Mustard.Contracts.Repository;
using Mustard.Contracts.Shared;
using Mustard.Shared;
using Mustard.SqlRepository.BizRepository;
using Mustard.SqlRepository.IdentityRepository;
using Ninject;
using Ninject.Web.Common;
using WebActivator;

[assembly: WebActivator.PreApplicationStartMethod(typeof (NinjectWebCommon), "Start")]
[assembly: ApplicationShutdownMethod(typeof (NinjectWebCommon), "Stop")]

namespace Mustard.Application
{
  public static class NinjectWebCommon
  {
    private static readonly Bootstrapper Bootstrapper = new Bootstrapper();

    /// <summary>
    ///   Starts the application
    /// </summary>
    [UsedImplicitly]
    public static void Start()
    {
      DynamicModuleUtility.RegisterModule(typeof (OnePerRequestHttpModule));
      DynamicModuleUtility.RegisterModule(typeof (NinjectHttpModule));
      Bootstrapper.Initialize(CreateKernel);
    }

    /// <summary>
    ///   Stops the application.
    /// </summary>
    [UsedImplicitly]
    public static void Stop()
    {
      Bootstrapper.ShutDown();
    }

    /// <summary>
    ///   Creates the kernel that will manage your application.
    /// </summary>
    /// <returns>The created kernel.</returns>
    private static IKernel CreateKernel()
    {
      var kernel = new StandardKernel();
      kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
      kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

      RegisterServices(kernel);
      return kernel;
    }

    /// <summary>
    ///   Load your modules or register your services here!
    /// </summary>
    /// <param name="kernel">The kernel.</param>
    private static void RegisterServices(IKernel kernel)
    {
      //      kernel.Bind<ILog>().ToMethod(ctx => LogManager.GetLogger(ctx.Request.Target.Type)); todo think about yagni

      // todo transient ��-�� ������� ���������� ����������� 
      kernel.Bind<IIdentityRepository>().To<IdentityRepository>().InTransientScope();
      kernel.Bind<IBizRepository>().To<BizRepository>().InTransientScope();

      kernel.Bind<IMapper>().To<CommonMapper>().InSingletonScope();

      kernel.Bind<IBizLayer>().To<BizLayer>().InTransientScope();
    }
  }
}