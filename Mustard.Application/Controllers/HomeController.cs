﻿using System.Linq;
using System.Web.Mvc;
using Mustard.Application.Helpers;
using Mustard.Models;
using Mustard.Resources.Application;
using Mustard.Shared;
using Mustard.Shared.Extensions;
using Mustard.ViewModels.Account;
using Mustard.ViewModels.Home;
using Mustard.ViewModels.Shared;

namespace Mustard.Application.Controllers
{
  public class HomeController : BaseController
  {
    public ActionResult Index()
    {
      var customers = BizRepository.CustomerCards.OrderBy(x => x.CompanyName)
                                   .AsEnumerable().Select(x => Mapper.Map<CustomerCard, CustomerCardViewModel>(x));
      return View(customers);
    }

    [Authorize(Roles = "Customer")]
    [HttpPost]
    public ActionResult AddOrder(AddOrderViewModel model) 
    {
      if (!ModelState.IsValid)
      {
        var err = new QueryResult("Invalid model", "args are invalid");
        return Json(err);
      }

      if (model.CustomerUserName != User.Identity.Name)
      {
        var err = new QueryResult("Invalid credintials", "Access not allowed");
        return Json(err);
      }

      CustomerCard customer = BizRepository.CustomerCards.FirstOrDefault(x => x.User.UserName == model.CustomerUserName);
      EmployeeCard manager = BizRepository.EmployeeCards.FirstOrDefault(x => x.User.UserName == model.ManagerUserName);

      if (customer == null || manager == null)
      {
        var err = new QueryResult("Invalid model", "args are invalid");
        return Json(err);
      }

      if (!BizLayer.CanAssignOrderToManager(manager.UserId))
      {
        var err = new QueryResult("Invalid query", Res.NoManagersAvailable);
        return Json(err);
      }

      var order = new Order
      {
        CustomerCard = customer,
        ManagerCard = manager,
        Description = model.Description
      };

      BizRepository.CreateOrder(order);

      var res = new QueryResult();
      return Json(res);
    }

    #region Partial helpers

    [HttpPost]
    public ActionResult CustomerOrdersTable(string customerUserName)
    {
      if (customerUserName.IsNullOrEmpty())
      {
        return View("Error");
      }

      CustomerCard customer = BizRepository.CustomerCards.FirstOrDefault(x => x.User.UserName == customerUserName);

      if (customer == null)
      {
        return View("Error");
      }

      var orders = customer.Orders.Select(x => Mapper.Map<Order, OrderViewModel>(x));
      var model = new CustomerOrdersViewModel
      {
        CustomerUserName = customerUserName,
        Orders = orders
      };

      return PartialView("_customerOrdersTable", model);
    }

    public ActionResult ManagerSlavesList(string managerUserName)
    {
      var manager = BizRepository.EmployeeCards.FirstOrDefault(x => x.User.UserName == managerUserName);
      var slaves = manager == null ? Enumerable.Empty<EmployeeCardViewModel>() :
                     manager.Slaves.Select(x => Mapper.Map<EmployeeCard, EmployeeCardViewModel>(x));

      return PartialView("_managerSlavesList", slaves);
    }

    [Authorize(Roles = "Customer")]
    public ActionResult AddOrderForm(string customerUserName)
    {
      if (customerUserName != User.Identity.Name)
      {
        return View("AccessNotAllowed");
      }

      // todo can fix to expression?
      var availableManagers = BizRepository.EmployeeCards.AsEnumerable()
                                           .Where(x => BizLayer.CanAssignOrderToManager(x.UserId))
                                           .Select(x => Builder.SelectListItem("{0} {1}".FormatWith(x.Surname, x.Name),
                                                                               x.User.UserName));

      var model = new AddOrderViewModel
      {
        CustomerUserName = customerUserName,
        AvailableManagers = availableManagers
      };

      return PartialView("_addOrderForm", model);
    }

    #endregion
  }
}