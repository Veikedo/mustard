﻿using System.Web.Mvc;

namespace Mustard.Application.Controllers
{
  public class CultureController : BaseController
  {
    public ActionResult ChangeLocale(string returnUrl)
    {
      Session["culture"] = CurrentCulture == "ru-RU" ? "en-US" : "ru-RU";

      if (Url.IsLocalUrl(returnUrl))
      {
        return Redirect(returnUrl);
      }

      return RedirectToAction("Index", "Home");
    }
  }
}