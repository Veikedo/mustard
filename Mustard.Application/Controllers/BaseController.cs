﻿using System.Globalization;
using System.Threading;
using System.Web.Mvc;
using System.Web.Routing;
using log4net;
using Mustard.Contracts.BusinessLayer;
using Mustard.Contracts.Repository;
using Mustard.Contracts.Shared;
using Mustard.Shared;

namespace Mustard.Application.Controllers
{
  public abstract class BaseController : Controller
  {
    protected readonly IBizRepository BizRepository;
    protected readonly IIdentityRepository IdentityRepository;
    protected readonly IMapper Mapper;
    protected readonly IBizLayer BizLayer;
    protected string CurrentCulture;

    protected BaseController()
    {
      IdentityRepository = DependencyResolver.Current.GetService<IIdentityRepository>();
      BizRepository = DependencyResolver.Current.GetService<IBizRepository>();
      BizLayer = DependencyResolver.Current.GetService<IBizLayer>();
      Mapper = DependencyResolver.Current.GetService<IMapper>();
    }

    protected override void Initialize(RequestContext requestContext)
    {
      base.Initialize(requestContext);

      CurrentCulture = Session["culture"] as string;

      if (string.IsNullOrEmpty(CurrentCulture) || !CultureHelper.CustomCultureExists(CurrentCulture))
      {
        Session["culture"] = CurrentCulture = "ru-RU";
      }

      Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(CurrentCulture); // todo resharper suck me
      Thread.CurrentThread.CurrentUICulture = new CultureInfo(CurrentCulture);
    }

    protected override void OnException(ExceptionContext filterContext)
    {
      base.OnException(filterContext);
    }
  }
}