﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Mustard.Identity;
using Mustard.Models;
using Mustard.Resources.Application;
using Mustard.Shared;
using Mustard.Shared.Extensions;
using Mustard.ViewModels.Account;
using Mustard.ViewModels.Shared;

namespace Mustard.Application.Controllers
{
  [Authorize]
  public class AccountController : BaseController
  {
    private const string ManagerNotSet = "@noManager";

    public AccountController()
    {
      var store = new UserStore(IdentityRepository, Mapper);
      UserManager = new UserManager(store, IdentityRepository, BizRepository);
    }

    private UserManager UserManager { get; set; }

    [AllowAnonymous]
    public ActionResult Login(string returnUrl)
    {
      ViewBag.ReturnUrl = returnUrl;
      return View();
    }

    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
    {
      if (ModelState.IsValid)
      {
        IdentityUser user = await UserManager.FindAsync(model.UserName, model.Password);
        if (user != null)
        {
          await SignInAsync(user, model.RememberMe);
          return RedirectToLocal(returnUrl);
        }

        ModelState.AddModelError("", Res.InvalidUsernameOrPassword);
      }

      // If we got this far, something failed, redisplay form
      return View(model);
    }

    [AllowAnonymous]
    public ActionResult Register()
    {
      return View();
    }

    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Register(RegisterViewModel model)
    {
      bool isCustomer;
      if (bool.TryParse(ModelState["IsCustomer"].Value.AttemptedValue, out isCustomer))
      {
        string key = isCustomer ? "Employee" : "Customer";

        //if customer then remove all errors related to employee
        foreach (ModelState state in ModelState.Where(x => x.Key.StartsWith(key)).Select(x => x.Value))
        {
          state.Errors.Clear();
        }
      }

      if (ModelState.IsValid)
      {
        var user = new IdentityUser {UserName = model.UserName, Email = model.Email};
        IdentityResult result = await UserManager.CreateAsync(user, model.Password);

        if (result.Succeeded)
        {
          if (isCustomer)
          {
            CustomerCard card = Mapper.Map<CustomerCardViewModel, CustomerCard>(model.CustomerCard);
            await UserManager.AddToCustomerRoleAsync(card, user.Id);
          }
          else
          {
            EmployeeCard card = Mapper.Map<EmployeeCardViewModel, EmployeeCard>(model.EmployeeCard);
            await UserManager.AddToEmployeeRoleAsync(card, user.Id);
          }

          await SignInAsync(user, isPersistent: false);
          return RedirectToAction("Index", "Home");
        }

        AddErrors(result);
      }

      // If we got this far, something failed, redisplay form
      return View(model);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult LogOff()
    {
      AuthenticationManager.SignOut();
      return RedirectToAction("Index", "Home");
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && UserManager != null)
      {
        UserManager.Dispose();
        UserManager = null;
      }

      base.Dispose(disposing);
    }

    public ActionResult Manage(ManageMessageId? message)
    {
      ViewBag.StatusMessage = message == ManageMessageId.ChangePasswordSuccess ? Res.YourPasswordHasBeenChanged :
                                message == ManageMessageId.Error ? Res.AnErrorHasOccurred :
                                  message == ManageMessageId.UpdatingInfoSuccess ? Res.InfoUpdated : string.Empty;

      ViewBag.ReturnUrl = Url.Action("Manage");

      var model = new ManageUserViewModel
      {
        ChangingPasswordModel = new ChangingPasswordViewModel(),
      };

      if (User.IsInRole("Employee"))
      {
        model.ManagingEmployeeModel = GetManagingEmployeeViewModel();
        model.ManagingType = ManagingType.UpdatingEmployeeInfo;
      }
      else
      {
        model.ManagingCustomerModel = GetManagingCustomerViewModel();
        model.ManagingType = ManagingType.UpdatingCustomerInfo;
      }

      return View(model);
    }

    [HttpPost]
    public async Task<ActionResult> ChangePassword(ChangingPasswordViewModel model)
    {
      ViewBag.ReturnUrl = Url.Action("Manage");

      if (ModelState.IsValid)
      {
        IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(),
                                                                      model.OldPassword,
                                                                      model.NewPassword);
        if (result.Succeeded)
        {
          return RedirectToAction("Manage", new {Message = ManageMessageId.ChangePasswordSuccess});
        }

        AddErrors(result);
      }

      var wrap = new ManageUserViewModel {ChangingPasswordModel = model};

      if (User.IsInRole("Employee"))
      {
        wrap.ManagingEmployeeModel = GetManagingEmployeeViewModel();
        wrap.ManagingType = ManagingType.UpdatingEmployeeInfo;
      }
      else
      {
        wrap.ManagingCustomerModel = GetManagingCustomerViewModel();
        wrap.ManagingType = ManagingType.UpdatingCustomerInfo;
      }

      return View("Manage", wrap);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult ManageEmployeeInfo(ManageEmployeeCardViewModel model)
    {
      if (!ModelState.IsValid)
      {
        model.AvailableManagers = GetAvailableManagersSelectList(model.UserName, model.ManagerUserName);
        var wrap = new ManageUserViewModel
        {
          ChangingPasswordModel = new ChangingPasswordViewModel(),
          ManagingEmployeeModel = GetManagingEmployeeViewModel(),
          ManagingType = ManagingType.UpdatingEmployeeInfo
        };

        return View("Manage", wrap);
      }

      if (User.Identity.Name != model.UserName)
      {
        return View("AccessNotAllowed");
      }

      EmployeeCard employee = BizRepository.EmployeeCards.FirstOrDefault(x => x.User.UserName == model.UserName);

      if (employee == null)
      {
        return View("Error");
      }

      EmployeeCard manager = employee.ManagerCard;

      if (model.ManagerUserName == ManagerNotSet)
      {
        if (manager != null)
        {
          manager.Slaves.Remove(employee);
          BizRepository.UpdateEmployeeCard(manager);
        }
      }
      else
      {
        // preventing if user just updated info
        if (manager == null || model.ManagerUserName != manager.User.UserName)
        {
          EmployeeCard newManager =
            BizRepository.EmployeeCards.FirstOrDefault(x => x.User.UserName == model.ManagerUserName);

          if (newManager == null)
          {
            return View("Error");
          }

          employee.ManagerCard = newManager;
        }
      }

      employee.Name = model.Name;
      employee.Surname = model.Surname;

      BizRepository.UpdateEmployeeCard(employee);

      return RedirectToAction("Manage", "Account", new {Message = ManageMessageId.UpdatingInfoSuccess});
    }

    public ActionResult ManageCustomerInfo(CustomerCardViewModel model)
    {
      if (!ModelState.IsValid)
      {
        var wrap = new ManageUserViewModel
        {
          ChangingPasswordModel = new ChangingPasswordViewModel(),
          ManagingCustomerModel = GetManagingCustomerViewModel(),
          ManagingType = ManagingType.UpdatingCustomerInfo
        };

        return View("Manage", wrap);
      }

      if (User.Identity.Name != model.UserName)
      {
        return View("AccessNotAllowed");
      }

      var customer = Mapper.Map<CustomerCardViewModel, CustomerCard>(model);

      var customerId = BizRepository.CustomerCards.First(x => x.User.UserName == model.UserName).UserId;
      customer.UserId = customerId;

      BizRepository.UpdateCustomerCard(customer);
      return RedirectToAction("Manage", "Account", new {Message = ManageMessageId.UpdatingInfoSuccess});
    }

    #region Helpers

    public enum ManageMessageId
    {
      ChangePasswordSuccess,
      Error,
      UpdatingInfoSuccess,
    }

    private IAuthenticationManager AuthenticationManager
    {
      get { return HttpContext.GetOwinContext().Authentication; }
    }

    private CustomerCardViewModel GetManagingCustomerViewModel()
    {
      var customer = BizRepository.CustomerCards.First(x => x.User.UserName == User.Identity.Name);
      return Mapper.Map<CustomerCard, CustomerCardViewModel>(customer);
    }

    private ManageEmployeeCardViewModel GetManagingEmployeeViewModel()
    {
      EmployeeCard employee = BizRepository.EmployeeCards.First(x => x.User.UserName == User.Identity.Name);
      ManageEmployeeCardViewModel model = Mapper.Map<EmployeeCard, ManageEmployeeCardViewModel>(employee);

      IEnumerable<SelectListItem> availableManagers = GetAvailableManagersSelectList(model.UserName,
                                                                                     model.ManagerUserName);
      model.AvailableManagers = availableManagers;

      return model;
    }

    private IEnumerable<SelectListItem> GetAvailableManagersSelectList(string employeeUserName,
                                                                       string currentManagerUserName)
    {
      var res = new List<SelectListItem> {Builder.SelectListItem(Res.ManagerNotSet, ManagerNotSet)};

      var list = BizRepository.EmployeeCards.AsEnumerable()
                              .Where(x => x.User.UserName != employeeUserName && BizLayer.CanAddSlaveToManager(x.UserId))
                              .Select(x => Builder.SelectListItem("{0} {1}".FormatWith(x.Surname, x.Name),
                                                                  x.User.UserName,
                                                                  x.User.UserName == currentManagerUserName));
      res.AddRange(list);
      return res;
    }

    private async Task SignInAsync(IdentityUser user, bool isPersistent)
    {
      AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
      ClaimsIdentity identity =
        await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
      AuthenticationManager.SignIn(new AuthenticationProperties {IsPersistent = isPersistent}, identity);
    }

    private void AddErrors(IdentityResult result)
    {
      foreach (string error in result.Errors)
      {
        ModelState.AddModelError("", error);
      }
    }

    private ActionResult RedirectToLocal(string returnUrl)
    {
      if (Url.IsLocalUrl(returnUrl))
      {
        return Redirect(returnUrl);
      }

      return RedirectToAction("Index", "Home");
    }

    #endregion
  }
}