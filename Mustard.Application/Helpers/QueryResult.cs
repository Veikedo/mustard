﻿namespace Mustard.Application.Helpers
{
  public class QueryResult
  {
    public QueryResult(string error, string message)
    {
      Error = error;
      Message = message;
    }

    public QueryResult()
    {
      Succeded = true;
    }

    public bool Succeded { get; set; }
    public string Error { get; set; }
    public string Message { get; set; }
  }
}