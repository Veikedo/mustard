﻿function getCustomerOrders(customerUserName, callback) {
  $.ajax({
    type: "POST",
    url: "/Home/CustomerOrdersTable",
    data: { customerUserName: customerUserName },
    success: function(data) {
      $("#orders-container").html(data);
    },
    error: function () {
      alert("An error occurred when getting customer orders");
    }
  });
}

function validateAddingOrderForm() {
  var valid = $("#description-area").val().length != 0;

  if (!valid) {
    $("#description-area").addClass("input-validation-error");
  } else {
    $("#description-area").removeClass("input-validation-error");
  }

  return valid;
}

function addOrder(order) {
  $.ajax({
    type: "POST",
    url: "/Home/AddOrder",
    data: order,
    success: function (result) {
      if (result.Succeded) {
        getCustomerOrders(order.customerUserName);
      } else {
        alert(result.Error + "<br />" + result.Message);
      }
    },
    error: function() {
      alert("An error occurred when adding customer order");
    }
  });
}

$('[data-class="customer-row"]').click(function() {
  $('[data-class="customer-row"]').removeClass("selected-customer");
  $(this).addClass("selected-customer");

  var customerUserName = $(this).data("username");

  getCustomerOrders(customerUserName);
  $("#orders-container").removeClass("hidden");
});


//$(document).on("ready", '[data-class="slaves-popover"]', popover); todo why it doesn't work?

