﻿using Microsoft.Owin;
using Mustard.Application;
using Owin;

[assembly: OwinStartup(typeof (Startup))]

namespace Mustard.Application
{
  public partial class Startup
  {
    public void Configuration(IAppBuilder app)
    {
      ConfigureAuth(app);
    }
  }
}