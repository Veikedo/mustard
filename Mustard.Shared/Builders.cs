﻿using System.Web.Mvc;

namespace Mustard.Shared
{
  public static class Builder
  {
    public static SelectListItem SelectListItem(string text, string value, bool? selected = null)
    {
      return new SelectListItem
      {
        Text = text,
        Value = value,
        Selected = selected ?? false
      };
    }
  }
}