﻿using System;
using AutoMapper;
using Mustard.Contracts.Shared;
using Mustard.Identity;
using Mustard.Models;
using Mustard.Shared.Annotations;
using Mustard.ViewModels.Account;
using Mustard.ViewModels.Home;
using Mustard.ViewModels.Shared;

namespace Mustard.Shared
{
  [UsedImplicitly]
  public class CommonMapper : IMapper
  {
    static CommonMapper()
    {
      InitMaps();
    }

    public object Map(object source, Type sourceType, Type destinationType)
    {
      return Mapper.Map(source, sourceType, destinationType);
    }

    public TDestination Map<TSource, TDestination>(TSource source)
    {
      return (TDestination) Mapper.Map(source, typeof (TSource), typeof (TDestination));
    }

    private static void InitMaps()
    {
      Mapper.CreateMap<IdentityUser, User>();
      Mapper.CreateMap<User, IdentityUser>();

      Mapper.CreateMap<CustomerCardViewModel, CustomerCard>();
      Mapper.CreateMap<CustomerCard, CustomerCardViewModel>()
            .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.User.UserName));

      Mapper.CreateMap<EmployeeCardViewModel, EmployeeCard>();
      Mapper.CreateMap<EmployeeCard, EmployeeCardViewModel>();

      Mapper.CreateMap<OrderViewModel, Order>();
      Mapper.CreateMap<Order, OrderViewModel>()
            .ForMember(dest => dest.CustomerUserName, opt => opt.MapFrom(src => src.CustomerCard.User.UserName))
            .ForMember(dest => dest.ManagerUserName, opt => opt.MapFrom(src => src.ManagerCard.User.UserName))
            .ForMember(dest => dest.ManagerName, opt => opt.MapFrom(src => src.ManagerCard.Name))
            .ForMember(dest => dest.ManagerSurname, opt => opt.MapFrom(src => src.ManagerCard.Surname));

      Mapper.CreateMap<EmployeeCard, ManageEmployeeCardViewModel>()
            .ForMember(dest => dest.ManagerUserName,
                       opt => opt.MapFrom(src => src.ManagerCard == null ? string.Empty : src.ManagerCard.User.UserName))
            .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.User.UserName));
    }
  }
}