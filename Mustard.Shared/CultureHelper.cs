﻿using System;
using System.Globalization;
using System.Linq;

namespace Mustard.Shared
{
  public static class CultureHelper
  {
    /// <summary>
    ///   Returns if the culture represented by <paramref name="code" /> exists
    /// </summary>
    /// <param name="code"></param>
    /// <returns></returns>
    public static bool CustomCultureExists(string code)
    {
      CultureInfo[] userCultures = CultureInfo.GetCultures(CultureTypes.AllCultures);
      return userCultures.Any(x => x.Name.Equals(code, StringComparison.OrdinalIgnoreCase));
    }
  }
}