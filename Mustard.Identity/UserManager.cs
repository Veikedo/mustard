﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Mustard.Contracts.Repository;
using Mustard.Models;
using Mustard.Resources.Identity;

namespace Mustard.Identity
{
  public class UserManager : UserManager<IdentityUser>
  {
    private const string CustomerRole = "Customer";
    private const string EmployeeRole = "Employee";

    private readonly IBizRepository _bizRepository;
    private readonly IIdentityRepository _identityRepository;

    public UserManager(IUserStore<IdentityUser> store, IIdentityRepository identityRepository,
                       IBizRepository bizRepository) : base(store)
    {
      _identityRepository = identityRepository;
      _bizRepository = bizRepository;
    }

    #region Overrides of UserManager<IdentityUser>

    public override Task<bool> IsInRoleAsync(string userId, string role)
    {
      return base.IsInRoleAsync(userId, role);
    }

    #endregion

    public override async Task<IdentityResult> CreateAsync(IdentityUser user, string password)
    {
      if (_identityRepository.Users.Any(x => x.Email.Equals(user.Email, StringComparison.OrdinalIgnoreCase)))
      {
        return await Task.FromResult(IdentityResult.Failed(ResIdentity.EmailAlreadyExistsError));
      }

      return await base.CreateAsync(user, password);
    }

    public override Task<IdentityResult> AddToRoleAsync(string userId, string role)
    {
      throw new NotSupportedException("Use specific methods");
    }

    public override Task<IdentityResult> RemoveFromRoleAsync(string userId, string role)
    {
      throw new NotSupportedException();
    }

    public async Task<IdentityResult> AddToCustomerRoleAsync(CustomerCard card, string userId)
    {
      IdentityResult result = await base.AddToRoleAsync(userId, CustomerRole);

      if (!result.Succeeded)
      {
        return result;
      }

      card.UserId = userId;
      _bizRepository.CreateCustomerCard(card);
      return await Task.FromResult(IdentityResult.Success);
    }

    public async Task<IdentityResult> AddToEmployeeRoleAsync(EmployeeCard card, string userId)
    {
      IdentityResult result = await base.AddToRoleAsync(userId, EmployeeRole);

      if (!result.Succeeded)
      {
        return result;
      }

      card.UserId = userId;
      _bizRepository.CreateEmployeeCard(card);
      return await Task.FromResult(IdentityResult.Success);
    }
  }
}