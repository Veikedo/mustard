﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Mustard.Contracts.Repository;
using Mustard.Contracts.Shared;
using Mustard.Models;

namespace Mustard.Identity
{
  public class UserStore : IUserPasswordStore<IdentityUser>, IUserRoleStore<IdentityUser>
  {
    private readonly IMapper _mapper;
    private readonly IIdentityRepository _repository;

    public UserStore(IIdentityRepository repository, IMapper mapper)
    {
      _repository = repository;
      _mapper = mapper;
    }

    #region Implementation of IDisposable

    public void Dispose()
    {
    }

    #endregion

    #region Implementation of IUserStore<IdentityUser>

    public async Task CreateAsync(IdentityUser user)
    {
      User repoUser = _mapper.Map<IdentityUser, User>(user);
      await Task.FromResult(_repository.CreateUser(repoUser));
    }

    public async Task UpdateAsync(IdentityUser user)
    {
      User repoUser = _mapper.Map<IdentityUser, User>(user);
      await Task.FromResult(_repository.UpdateUser(repoUser));
    }

    public async Task DeleteAsync(IdentityUser user)
    {
      await Task.Run(() => _repository.RemoveUser(user.Id));
    }

    public async Task<IdentityUser> FindByIdAsync(string userId)
    {
      User repoUser = _repository.Users.FirstOrDefault(x => x.Id == userId);

      if (repoUser != null)
      {
        IdentityUser user = _mapper.Map<User, IdentityUser>(repoUser);
        return await Task.FromResult(user);
      }

      return await Task.FromResult<IdentityUser>(null);
    }

    public async Task<IdentityUser> FindByNameAsync(string userName)
    {
      User repoUser = _repository.Users.FirstOrDefault(x => x.UserName == userName);

      if (repoUser != null)
      {
        IdentityUser user = _mapper.Map<User, IdentityUser>(repoUser);
        return await Task.FromResult(user);
      }

      return await Task.FromResult<IdentityUser>(null);
    }

    #endregion

    #region Implementation of IUserPasswordStore<IdentityUser>

    public async Task SetPasswordHashAsync(IdentityUser user, string passwordHash)
    {
      user.PasswordHash = passwordHash;
      await Task.FromResult<object>(null);
    }

    public async Task<string> GetPasswordHashAsync(IdentityUser user)
    {
      return await Task.FromResult(user.PasswordHash);
    }

    public async Task<bool> HasPasswordAsync(IdentityUser user)
    {
      // Users will have password anyway
      return await Task.FromResult(true);
    }

    #endregion

    #region Implementation of IUserRoleStore<IdentityUser>

    public async Task AddToRoleAsync(IdentityUser user, string roleCode)
    {
      // todo Should I verify user != null? 
      User repoUser = _repository.Users.FirstOrDefault(x => x.Id == user.Id);

      if (repoUser != null)
      {
        Role role = _repository.Roles.FirstOrDefault(x => x.Code == roleCode);

        if (role != null)
        {
          repoUser.Roles.Add(role);
          await Task.Run(() => _repository.ApplyChanges());
          return;
        }
      }

      await Task.FromResult<object>(null);
    }

    public async Task RemoveFromRoleAsync(IdentityUser user, string roleCode)
    {
      // Should I verify user != null? 
      User repoUser = _repository.Users.FirstOrDefault(x => x.Id == user.Id);

      if (repoUser != null)
      {
        Role role = _repository.Roles.FirstOrDefault(x => x.Code == roleCode);

        if (role != null)
        {
          repoUser.Roles.Remove(role);
          await Task.Run(() => _repository.ApplyChanges());
          return;
        }
      }

      await Task.FromResult<object>(null);
    }

    public async Task<IList<string>> GetRolesAsync(IdentityUser user)
    {
      User repoUser = _repository.Users.FirstOrDefault(x => x.Id == user.Id);

      if (repoUser != null)
      {
        return await Task.Run<IList<string>>(() => repoUser.Roles.Select(x => x.Code).ToList());
      }

      return await Task.FromResult<IList<string>>(null);
    }

    public async Task<bool> IsInRoleAsync(IdentityUser user, string role)
    {
      User repoUser = _repository.Users.FirstOrDefault(x => x.Id == user.Id);

      if (repoUser != null)
      {
        return await Task.Run(() => repoUser.Roles.Any(x => x.Code == role));
      }

      return await Task.FromResult(false);
    }

    #endregion
  }
}