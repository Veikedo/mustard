﻿using System;
using Microsoft.AspNet.Identity;

namespace Mustard.Identity
{
  public class IdentityUser : IUser
  {
    public IdentityUser()
    {
      Id = Guid.NewGuid().ToString("N");
    }

    public string PasswordHash { get; set; }
    public string Email { get; set; }
    public string Id { get; set; }
    public string UserName { get; set; }
  }
}