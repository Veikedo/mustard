﻿using System.Collections.Generic;

namespace Mustard.ViewModels.Home
{
  public class OrderViewModel
  {
    public string Description { get; set; }
    public string ManagerName { get; set; }
    public string ManagerSurname { get; set; }
    public string ManagerUserName { get; set; }
    public string CustomerUserName { get; set; }
  }
}