﻿using System.Collections.Generic;
using System.Linq;

namespace Mustard.ViewModels.Home
{
  public class ManagerViewModel
  {
    public ManagerViewModel()
    {
      Slaves = Enumerable.Empty<ManagerViewModel>();
    }

    public string ManagerUserName { get; set; }
    public string ManagerName { get; set; }
    public string ManagerSurname { get; set; }
    public IEnumerable<ManagerViewModel> Slaves { get; set; }
  }
}