﻿using System.Collections.Generic;

namespace Mustard.ViewModels.Home
{
  public class CustomerOrdersViewModel
  {
    public string CustomerUserName { get; set; }
    public IEnumerable<OrderViewModel> Orders { get; set; }
  }
}