﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Mustard.ViewModels.Home
{
  public class AddOrderViewModel
  {
    public string CustomerUserName { get; set; }
    public string ManagerUserName { get; set; }
    public string Description { get; set; }
    public IEnumerable<SelectListItem> AvailableManagers { get; set; }
  }
}