using System.ComponentModel.DataAnnotations;
using Mustard.Resources.Application;
using Mustard.Resources.ViewModel;

namespace Mustard.ViewModels.Account
{
  public class ChangingPasswordViewModel
  {
    [Required(ErrorMessageResourceType = typeof (ResViewModel), ErrorMessageResourceName = "FieldIsRequired")]
    [DataType(DataType.Password)]
    [Display(ResourceType = typeof (Res), Name = "CurrentPassword")]
    public string OldPassword { get; set; }

    [Required(ErrorMessageResourceType = typeof (ResViewModel), ErrorMessageResourceName = "FieldIsRequired")]
    [StringLength(100, ErrorMessageResourceType = typeof (ResViewModel),
      ErrorMessageResourceName = "PasswordLengthError", MinimumLength = 6)]
    [DataType(DataType.Password)]
    [Display(ResourceType = typeof (ResViewModel), Name = "NewPassword")]
    public string NewPassword { get; set; }

    [DataType(DataType.Password)]
    [Display(ResourceType = typeof (ResViewModel), Name = "ConfirmNewPassword")]
    [Compare("NewPassword", ErrorMessageResourceType = typeof (ResViewModel),
      ErrorMessageResourceName = "PasswDontMatchConfirmation")]
    public string ConfirmPassword { get; set; }
  }
}