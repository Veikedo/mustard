using System.ComponentModel.DataAnnotations;
using Mustard.Resources.ViewModel;
using Mustard.ViewModels.Annotations;

namespace Mustard.ViewModels.Account
{
  [UsedImplicitly]
  public class CustomerCardViewModel
  {
    public string UserName { get; set; }
    
    [Required(ErrorMessageResourceType = typeof (ResViewModel), ErrorMessageResourceName = "FieldIsRequired")]
    [Display(ResourceType = typeof (ResViewModel), Name = "Company")]
    public string CompanyName { get; set; }

    [Required(ErrorMessageResourceType = typeof (ResViewModel), ErrorMessageResourceName = "FieldIsRequired")]
    [Display(ResourceType = typeof (ResViewModel), Name = "Street")]
    public string Street { get; set; }

    [Required(ErrorMessageResourceType = typeof (ResViewModel), ErrorMessageResourceName = "FieldIsRequired")]
    [Display(ResourceType = typeof (ResViewModel), Name = "House")]
    public string House { get; set; }
  }
}