﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Mustard.Resources.ViewModel;

namespace Mustard.ViewModels.Account
{
  public class ManageEmployeeCardViewModel
  {
    [Required]
    public string UserName { get; set; }

    [Required]
    public string ManagerUserName { get; set; }

    [Required(ErrorMessageResourceType = typeof (ResViewModel), ErrorMessageResourceName = "FieldIsRequired")]
    [Display(ResourceType = typeof (ResViewModel), Name = "Name")]
    public string Name { get; set; }

    [Required(ErrorMessageResourceType = typeof (ResViewModel), ErrorMessageResourceName = "FieldIsRequired")]
    [Display(ResourceType = typeof (ResViewModel), Name = "Surname")]
    public string Surname { get; set; }
    public IEnumerable<SelectListItem> AvailableManagers { get; set; }
  }
}