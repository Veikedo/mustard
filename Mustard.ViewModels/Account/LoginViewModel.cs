﻿using System.ComponentModel.DataAnnotations;
using Mustard.Resources.Application;
using Mustard.Resources.ViewModel;

namespace Mustard.ViewModels.Account
{
  public class LoginViewModel
  {
    [Required]
    [Display(ResourceType = typeof(ResViewModel), Name = "UserName")]
    public string UserName { get; set; }

    [Required]
    [DataType(DataType.Password)]
    [Display(ResourceType = typeof(ResViewModel), Name = "Password")]
    public string Password { get; set; }

    [Display(ResourceType = typeof(ResViewModel), Name = "RememberMe")]
    public bool RememberMe { get; set; }
  }
}