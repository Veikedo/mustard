using System.ComponentModel.DataAnnotations;
using Mustard.Resources.ViewModel;
using Mustard.ViewModels.Shared;

namespace Mustard.ViewModels.Account
{
  public class RegisterViewModel
  {
    [Required(ErrorMessageResourceType = typeof (ResViewModel), ErrorMessageResourceName = "FieldIsRequired")]
    [Display(ResourceType = typeof (ResViewModel), Name = "UserName")]
    public string UserName { get; set; }

    [Required(ErrorMessageResourceType = typeof (ResViewModel), ErrorMessageResourceName = "FieldIsRequired")]
    [StringLength(100, ErrorMessageResourceType = typeof (ResViewModel), ErrorMessageResourceName = "PasswordLengthError",
      MinimumLength = 6)]
    [DataType(DataType.Password)]
    [Display(ResourceType = typeof (ResViewModel), Name = "Password")]
    public string Password { get; set; }

    [DataType(DataType.Password)]
    [Display(ResourceType = typeof (ResViewModel), Name = "ConfirmPassword")]
    [Compare("Password", ErrorMessageResourceType = typeof (ResViewModel),
      ErrorMessageResourceName = "PasswDontMatchConfirmation")]
    public string ConfirmPassword { get; set; }

    [Required(ErrorMessageResourceType = typeof (ResViewModel), ErrorMessageResourceName = "FieldIsRequired")]
    [DataType(DataType.EmailAddress)]
    [RegularExpression(@".+@.+\..+", ErrorMessageResourceType = typeof (ResViewModel),
      ErrorMessageResourceName = "EmailNotValid")]
    public string Email { get; set; }

    public bool IsCustomer { get; set; }

    public CustomerCardViewModel CustomerCard { get; set; }
    public EmployeeCardViewModel EmployeeCard { get; set; }
  }
}