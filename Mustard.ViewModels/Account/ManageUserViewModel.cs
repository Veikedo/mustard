﻿namespace Mustard.ViewModels.Account
{
  public class ManageUserViewModel
  {
    public CustomerCardViewModel ManagingCustomerModel { get; set; }
    public ManageEmployeeCardViewModel ManagingEmployeeModel { get; set; }
    public ChangingPasswordViewModel ChangingPasswordModel { get; set; }

    public ManagingType ManagingType { get; set; }
  }

  public enum ManagingType
  {
    UpdatingEmployeeInfo,
    UpdatingCustomerInfo
  }
}