using System.ComponentModel.DataAnnotations;
using Mustard.Resources.ViewModel;
using Mustard.ViewModels.Annotations;

namespace Mustard.ViewModels.Shared
{
  [UsedImplicitly]
  public class EmployeeCardViewModel
  {
    [Required(ErrorMessageResourceType = typeof (ResViewModel), ErrorMessageResourceName = "FieldIsRequired")]
    [Display(ResourceType = typeof (ResViewModel), Name = "Name")]
    public string Name { get; set; }

    [Required(ErrorMessageResourceType = typeof (ResViewModel), ErrorMessageResourceName = "FieldIsRequired")]
    [Display(ResourceType = typeof (ResViewModel), Name = "Surname")]
    public string Surname { get; set; }
  }
}