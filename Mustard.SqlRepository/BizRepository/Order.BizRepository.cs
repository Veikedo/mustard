﻿using System.Data.Entity;
using System.Linq;
using Mustard.Models;

namespace Mustard.SqlRepository.BizRepository
{
  public partial class BizRepository
  {
    public IQueryable<Order> Orders
    {
      get { return Db.Orders; }
    }

    public bool CreateOrder(Order instance)
    {
      if (instance.Id == 0)
      {
        Db.Orders.Add(instance);
        Db.SaveChanges();
        return true;
      }

      return false;
    }

    public bool UpdateOrder(Order instance)
    {
      Order cache = Db.Orders.FirstOrDefault(p => p.Id == instance.Id);

      if (cache != null)
      {
        cache.CustomerCardId = instance.CustomerCardId;
        cache.ManagerCardId = instance.ManagerCardId;
        cache.Description = instance.Description;

        Db.Entry(cache).State = EntityState.Modified;
        Db.SaveChanges();
        return true;
      }

      return false;
    }

    public bool RemoveOrder(int idOrder)
    {
      Order instance = Db.Orders.FirstOrDefault(p => p.Id == idOrder);

      if (instance != null)
      {
        Db.Orders.Remove(instance);
        Db.SaveChanges();
        return true;
      }

      return false;
    }
  }
}