﻿using System.Data.Entity;
using System.Linq;
using Mustard.Models;

namespace Mustard.SqlRepository.BizRepository
{
  public partial class BizRepository
  {
    public IQueryable<EmployeeCard> EmployeeCards
    {
      get { return Db.EmployeeCards; }
    }

    public bool CreateEmployeeCard(EmployeeCard instance)
    {
      if (!string.IsNullOrEmpty(instance.UserId))
      {
        Db.EmployeeCards.Add(instance);
        Db.SaveChanges();
        return true;
      }

      return false;
    }

    public bool UpdateEmployeeCard(EmployeeCard instance)
    {
      int res = Db.Database
                  .ExecuteSqlCommand("UPDATE EmployeeCards " +
                                     "SET ManagerCardId=@p0, Version=@p1, Name=@p2, Surname=@p3 " +
                                     "WHERE UserId=@p4 AND Version=@p5",
                                     instance.ManagerCardId, instance.Version + 1, instance.Name, instance.Surname,
                                     instance.UserId, instance.Version);

      if (res > 0)
      {
        EmployeeCard updatedCard = Db.EmployeeCards.First(x => x.UserId == instance.UserId); // ef needs to know that 
        Db.Entry(updatedCard).State = EntityState.Modified;
        Db.SaveChanges();
      }

      return res > 0;
    }

    public bool RemoveEmployeeCard(string idEmployeeCard)
    {
      EmployeeCard instance = Db.EmployeeCards.FirstOrDefault(p => p.UserId == idEmployeeCard);

      if (instance != null)
      {
        Db.EmployeeCards.Remove(instance);
        Db.SaveChanges();
        return true;
      }

      return false;
    }
  }
}