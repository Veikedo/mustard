﻿using Mustard.Contracts.Repository;
using Mustard.SqlRepository.Annotations;

namespace Mustard.SqlRepository.BizRepository
{
  [UsedImplicitly]
  public partial class BizRepository : BaseRepository, IBizRepository
  {
    //    public BizRepository(MustardDbContext db) : base(db)
    //    {
    //    }
    public BizRepository()
    {
    }
  }
}