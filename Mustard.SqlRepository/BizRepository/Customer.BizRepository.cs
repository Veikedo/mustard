﻿using System.Data.Entity;
using System.Linq;
using Mustard.Models;

namespace Mustard.SqlRepository.BizRepository
{
  public partial class BizRepository
  {
    public IQueryable<CustomerCard> CustomerCards
    {
      get { return Db.CustomerCards; }
    }

    public bool CreateCustomerCard(CustomerCard instance)
    {
      if (!string.IsNullOrEmpty(instance.UserId))
      {
        Db.CustomerCards.Add(instance);
        Db.SaveChanges();

        return true;
      }

      return false;
    }

    public bool UpdateCustomerCard(CustomerCard instance)
    {
      CustomerCard cache = Db.CustomerCards.FirstOrDefault(p => p.UserId == instance.UserId);

      if (cache != null)
      {
        cache.CompanyName = instance.CompanyName;
        cache.Street = instance.Street;
        cache.House = instance.House;

        Db.Entry(cache).State = EntityState.Modified;
        Db.SaveChanges();
        return true;
      }

      return false;
    }

    public bool RemoveCustomerCard(string idCustomerCard)
    {
      CustomerCard instance = Db.CustomerCards.FirstOrDefault(p => p.UserId == idCustomerCard);

      if (instance != null)
      {
        Db.CustomerCards.Remove(instance);
        Db.SaveChanges();
        return true;
      }

      return false;
    }
  }
}