﻿using System.Data.Entity;
using System.Linq;
using Mustard.Models;

namespace Mustard.SqlRepository.IdentityRepository
{
  public partial class IdentityRepository
  {
    public IQueryable<User> Users
    {
      get { return Db.Users; }
    }

    public bool CreateUser(User instance)
    {
      if (!string.IsNullOrEmpty(instance.Id))
      {
        Db.Users.Add(instance);
        Db.SaveChanges();
        return true;
      }

      return false;
    }

    public bool UpdateUser(User instance)
    {
      User cache = Db.Users.FirstOrDefault(p => p.Id == instance.Id);

      if (cache != null)
      {
        cache.Email = instance.Email;
        cache.PasswordHash = instance.PasswordHash;

        Db.Entry(cache).State = EntityState.Modified;
        Db.SaveChanges();
        return true;
      }

      return false;
    }

    public bool RemoveUser(string idUser)
    {
      User instance = Db.Users.FirstOrDefault(p => p.Id == idUser);

      if (instance != null)
      {
        Db.Users.Remove(instance);
        Db.SaveChanges();
        return true;
      }

      return false;
    }
  }
}