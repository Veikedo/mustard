﻿using System.Data.Entity;
using System.Linq;
using Mustard.Models;

namespace Mustard.SqlRepository.IdentityRepository
{
  public partial class IdentityRepository
  {
    public IQueryable<Role> Roles
    {
      get { return Db.Roles; }
    }

    public bool CreateRole(Role instance)
    {
      if (!string.IsNullOrEmpty(instance.Id))
      {
        Db.Roles.Add(instance);
        Db.SaveChanges();
        return true;
      }

      return false;
    }

    public bool UpdateRole(Role instance)
    {
      Role cache = Db.Roles.FirstOrDefault(p => p.Id == instance.Id);

      if (cache != null)
      {
        cache.Code = instance.Code;

        Db.Entry(cache).State = EntityState.Modified;
        Db.SaveChanges();
        return true;
      }

      return false;
    }

    public bool RemoveRole(string idRole)
    {
      Role instance = Db.Roles.FirstOrDefault(p => p.Id == idRole);

      if (instance != null)
      {
        Db.Roles.Remove(instance);
        Db.SaveChanges();
        return true;
      }

      return false;
    }
  }
}