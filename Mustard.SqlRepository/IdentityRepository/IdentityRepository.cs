﻿using Mustard.Contracts.Repository;
using Mustard.SqlRepository.Annotations;

namespace Mustard.SqlRepository.IdentityRepository
{
  [UsedImplicitly]
  public partial class IdentityRepository : BaseRepository, IIdentityRepository
  {
    //    public IdentityRepository(MustardDbContext db) : base(db)
    //    {
    //    }
    public IdentityRepository()
    {
    }
  }
}