﻿using System.Data.Entity;
using System.Linq;
using Mustard.Models;

namespace Mustard.SqlRepository.IdentityRepository
{
  public partial class IdentityRepository
  {
    public IQueryable<PasswordResetToken> PasswordResetTokens
    {
      get { return Db.PasswordResetTokens; }
    }

    public bool CreatePasswordResetToken(PasswordResetToken instance)
    {
      if (!string.IsNullOrEmpty(instance.UserId))
      {
        Db.PasswordResetTokens.Add(instance);
        Db.SaveChanges();
        return true;
      }

      return false;
    }

    public bool UpdatePasswordResetToken(PasswordResetToken instance)
    {
      PasswordResetToken cache = Db.PasswordResetTokens.FirstOrDefault(p => p.UserId == instance.UserId);

      if (cache != null)
      {
        //TODO : Update fields for PasswordResetToken

        Db.Entry(cache).State = EntityState.Modified;
        Db.SaveChanges();
        return true;
      }

      return false;
    }

    public bool RemovePasswordResetToken(string idPasswordResetToken)
    {
      PasswordResetToken instance = Db.PasswordResetTokens.FirstOrDefault(p => p.UserId == idPasswordResetToken);

      if (instance != null)
      {
        Db.PasswordResetTokens.Remove(instance);
        Db.SaveChanges();
        return true;
      }

      return false;
    }
  }
}