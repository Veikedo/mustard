﻿using System.Data.Entity;
using Mustard.Models;
using Mustard.SqlRepository.Annotations;
using Mustard.SqlRepository.Migrations;

namespace Mustard.SqlRepository
{
  [UsedImplicitly]
  public class MustardDbContext : DbContext
  {
    public MustardDbContext() : base("name=MustardDb")
    {
      Database.SetInitializer(new MigrateDatabaseToLatestVersion<MustardDbContext, Configuration>());
    }

    public DbSet<User> Users { get; set; }
    public DbSet<CustomerCard> CustomerCards { get; set; }
    public DbSet<EmployeeCard> EmployeeCards { get; set; }
    public DbSet<Order> Orders { get; set; }
    public DbSet<Role> Roles { get; set; }
    public DbSet<PasswordResetToken> PasswordResetTokens { get; set; }
  }
}