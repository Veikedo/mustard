using System.Data.Entity.Migrations;
using Mustard.Models;

namespace Mustard.SqlRepository.Migrations
{
  internal sealed class Configuration : DbMigrationsConfiguration<MustardDbContext>
  {
    public Configuration()
    {
      AutomaticMigrationsEnabled = true;
      AutomaticMigrationDataLossAllowed = true;
    }

    protected override void Seed(MustardDbContext context)
    {
      context.Roles.AddOrUpdate(x => x.Code,
                                new Role {Code = "Customer"},
                                new Role {Code = "Employee"});
    }
  }
}