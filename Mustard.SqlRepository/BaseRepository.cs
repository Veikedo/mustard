﻿using Mustard.Contracts.Repository;

namespace Mustard.SqlRepository
{
  public abstract class BaseRepository : IRepository
  {
    protected readonly MustardDbContext Db; // TODO should I use property here?

    protected BaseRepository(MustardDbContext db)
    {
      Db = db;
    }

    protected BaseRepository() : this(new MustardDbContext())
    {
    }

    public void ApplyChanges()
    {
      Db.SaveChanges();
    }
  }
}