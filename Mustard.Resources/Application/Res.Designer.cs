﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34003
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Mustard.Resources.Application {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Res {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Res() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Mustard.Resources.Application.Res", typeof(Res).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Доступ запрещен.
        /// </summary>
        public static string AccessNotAllowed {
            get {
                return ResourceManager.GetString("AccessNotAllowed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Добавить.
        /// </summary>
        public static string Add {
            get {
                return ResourceManager.GetString("Add", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Адрес.
        /// </summary>
        public static string Address {
            get {
                return ResourceManager.GetString("Address", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Произошла ошибка.
        /// </summary>
        public static string AnErrorHasOccurred {
            get {
                return ResourceManager.GetString("AnErrorHasOccurred", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Сменить пароль.
        /// </summary>
        public static string ChangePassword {
            get {
                return ResourceManager.GetString("ChangePassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Форма смены пароля.
        /// </summary>
        public static string ChangePasswordForm {
            get {
                return ResourceManager.GetString("ChangePasswordForm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Компания.
        /// </summary>
        public static string Company {
            get {
                return ResourceManager.GetString("Company", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Подтвердите новый пароль.
        /// </summary>
        public static string ConfirmNewPassword {
            get {
                return ResourceManager.GetString("ConfirmNewPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Подтвердите пароль.
        /// </summary>
        public static string ConfirmPassword {
            get {
                return ResourceManager.GetString("ConfirmPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Создать новый аккаунт.
        /// </summary>
        public static string CreateNewAccount {
            get {
                return ResourceManager.GetString("CreateNewAccount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Текущий пароль.
        /// </summary>
        public static string CurrentPassword {
            get {
                return ResourceManager.GetString("CurrentPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Заказчик.
        /// </summary>
        public static string Customer {
            get {
                return ResourceManager.GetString("Customer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Заказчики.
        /// </summary>
        public static string Customers {
            get {
                return ResourceManager.GetString("Customers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Описание.
        /// </summary>
        public static string Description {
            get {
                return ResourceManager.GetString("Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email.
        /// </summary>
        public static string Email {
            get {
                return ResourceManager.GetString("Email", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Такой email уже зарегистрирован.
        /// </summary>
        public static string EmailAlreadyExistsError {
            get {
                return ResourceManager.GetString("EmailAlreadyExistsError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email некорректен.
        /// </summary>
        public static string EmailNotValid {
            get {
                return ResourceManager.GetString("EmailNotValid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Работник.
        /// </summary>
        public static string Employee {
            get {
                return ResourceManager.GetString("Employee", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ошибка.
        /// </summary>
        public static string Error {
            get {
                return ResourceManager.GetString("Error", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Привет, .
        /// </summary>
        public static string Hello {
            get {
                return ResourceManager.GetString("Hello", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Главная.
        /// </summary>
        public static string HomeLink {
            get {
                return ResourceManager.GetString("HomeLink", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Дом.
        /// </summary>
        public static string House {
            get {
                return ResourceManager.GetString("House", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Информация успешна обновлена.
        /// </summary>
        public static string InfoUpdated {
            get {
                return ResourceManager.GetString("InfoUpdated", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Неправильное имя пользователя или пароль.
        /// </summary>
        public static string InvalidUsernameOrPassword {
            get {
                return ResourceManager.GetString("InvalidUsernameOrPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Вход.
        /// </summary>
        public static string Login {
            get {
                return ResourceManager.GetString("Login", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Вход.
        /// </summary>
        public static string LoginTitle {
            get {
                return ResourceManager.GetString("LoginTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выйти.
        /// </summary>
        public static string LogOff {
            get {
                return ResourceManager.GetString("LogOff", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Управление аккаунтом.
        /// </summary>
        public static string ManageAccount {
            get {
                return ResourceManager.GetString("ManageAccount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Менеджер.
        /// </summary>
        public static string Manager {
            get {
                return ResourceManager.GetString("Manager", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Нет менеджера.
        /// </summary>
        public static string ManagerNotSet {
            get {
                return ResourceManager.GetString("ManagerNotSet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Имя.
        /// </summary>
        public static string Name {
            get {
                return ResourceManager.GetString("Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Новый пароль.
        /// </summary>
        public static string NewPassword {
            get {
                return ResourceManager.GetString("NewPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Нет.
        /// </summary>
        public static string No {
            get {
                return ResourceManager.GetString("No", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Вы не можете добавить заказ, так как все менеджеры невероятно заняты.
        /// </summary>
        public static string NoManagersAvailable {
            get {
                return ResourceManager.GetString("NoManagersAvailable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Заказы.
        /// </summary>
        public static string Orders {
            get {
                return ResourceManager.GetString("Orders", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Пароль.
        /// </summary>
        public static string Password {
            get {
                return ResourceManager.GetString("Password", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Регистрация.
        /// </summary>
        public static string Register {
            get {
                return ResourceManager.GetString("Register", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Запомнить?.
        /// </summary>
        public static string RememberMe {
            get {
                return ResourceManager.GetString("RememberMe", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Обязательно!.
        /// </summary>
        public static string Required {
            get {
                return ResourceManager.GetString("Required", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Подчиненные.
        /// </summary>
        public static string Slaves {
            get {
                return ResourceManager.GetString("Slaves", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Улица.
        /// </summary>
        public static string Street {
            get {
                return ResourceManager.GetString("Street", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Фамилия.
        /// </summary>
        public static string Surname {
            get {
                return ResourceManager.GetString("Surname", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Обновить.
        /// </summary>
        public static string Update {
            get {
                return ResourceManager.GetString("Update", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Обновить информацию.
        /// </summary>
        public static string UpdateInfo {
            get {
                return ResourceManager.GetString("UpdateInfo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Имя пользователя.
        /// </summary>
        public static string UserName {
            get {
                return ResourceManager.GetString("UserName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Вы вошли как .
        /// </summary>
        public static string YoureLoggedAs {
            get {
                return ResourceManager.GetString("YoureLoggedAs", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ваш пароль изменен.
        /// </summary>
        public static string YourPasswordHasBeenChanged {
            get {
                return ResourceManager.GetString("YourPasswordHasBeenChanged", resourceCulture);
            }
        }
    }
}
