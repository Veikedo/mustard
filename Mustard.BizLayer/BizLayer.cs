﻿using System;
using System.Diagnostics.Contracts;
using System.Linq;
using Mustard.BusinessLayer.Annotations;
using Mustard.Contracts.BusinessLayer;
using Mustard.Contracts.Repository;
using Mustard.Models;

namespace Mustard.BusinessLayer
{
  [UsedImplicitly]
  public class BizLayer : IBizLayer
  {
    private const int MaxAssignedOrders = 2;
    private const int MaxManagerSlaves = 2;
    private readonly IBizRepository _bizRepository;

    public BizLayer(IBizRepository bizRepository)
    {
      _bizRepository = bizRepository;
    }

    public bool CanAssignOrderToManager(string managerUserId)
    {
      Contract.Requires(!string.IsNullOrEmpty(managerUserId)); // todo install it

      EmployeeCard manager = _bizRepository.EmployeeCards.FirstOrDefault(x => x.UserId == managerUserId);

      if (manager == null)
      {
        throw new ArgumentException("There is no manager with the same Id", "managerUserId");
      }

      return manager.Orders.Count <= MaxAssignedOrders;
    }

    public bool CanAddSlaveToManager(string managerUserId)
    {
      Contract.Requires(!string.IsNullOrEmpty(managerUserId)); // todo install it

      EmployeeCard manager = _bizRepository.EmployeeCards.FirstOrDefault(x => x.UserId == managerUserId);

      if (manager == null)
      {
        throw new ArgumentException("There is no manager with the same Id", "managerUserId");
      }

      return manager.Slaves.Count <= MaxManagerSlaves;
    }
  }
}